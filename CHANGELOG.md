## [1.1.17](https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/compare/v1.1.16...v1.1.17) (2021-11-29)


### Bug Fixes

* add cpx and fix changelog error ([07bc040](https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/commits/07bc04046a33f2f3eb424e5054ed46208cee2a82))
* typo - git push [skip ci] ([8ae5ce5](https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/commits/8ae5ce56c1d1cdeb6c0436166738c3f05ef4a0b5))
* typo - git push [skip ci] ([8ae5ce5](https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/commits/8ae5ce56c1d1cdeb6c0436166738c3f05ef4a0b5))
